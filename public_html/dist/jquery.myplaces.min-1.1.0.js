var somospnt={};
somospnt.util={};
somospnt.util.ui={};
somospnt.util.ui.filter=(function(){var a={};
function d(f){a=f;
filterTemplate=$.templates("#"+a.filterTemplateId);
e(a.places);
$("."+a.filterContainerClass).on("click",b)
}function e(f){var g=c(f);
$("."+a.filterMainContainerClass).append(filterTemplate.render(g))
}function c(g){var k=[];
if(g){for(var h=0;
h<g.length;
h++){if(g[h].tags){for(var f=0;
f<g[h].tags.length;
f++){if($.inArray(g[h].tags[f],k)===-1){k.push(g[h].tags[f])
}}}}}return k
}function b(f){var g=[];
$(this).toggleClass(a.filterActiveClass);
$("."+a.filterActiveClass).each(function(h,i){g.push($(i).attr("id"))
});
somospnt.util.ui.map.filterLocations(g);
f.stopPropagation();
f.preventDefault()
}return{init:d}
})();
somospnt.util.ui.map=(function(){var d,h,i,f;
var k=[];
function q(s,t,r){f=$.templates(t);
n(r);
g(s)
}function n(r){d=new google.maps.Map($(".myplaces-map").get(0),r);
h=new google.maps.InfoWindow();
i=new google.maps.Marker({map:d});
google.maps.event.addListener(d,"idle",c)
}function g(r){k=[];
for(var s=0;
s<r.length;
s++){(function(){var t={};
t.place=r[s];
t.place.id=s;
t.marker=new google.maps.Marker({position:new google.maps.LatLng(r[s].lat,r[s].lng),map:d,title:r[s].name,icon:r[s].mapIcon});
k.push(t);
google.maps.event.addListener(t.marker,"click",function(){somospnt.util.ui.places.selecteById(t.place.id)
})
})()
}}function c(){var s=a();
var r=somospnt.util.ui.places.getSelectedLocation();
if(r&&$.inArray(r,s)===-1){s.unshift(r)
}somospnt.util.ui.places.showLocations(s)
}function b(){var s=a();
var r=somospnt.util.ui.places.getSelectedLocation();
if(r&&$.inArray(r,s)===-1){r.infowindow=null;
h.close();
somospnt.util.ui.places.unselect()
}somospnt.util.ui.places.showLocations(s)
}function a(){var t=[];
var s=d.getBounds();
for(var r=0;
r<k.length;
r++){if(k[r].marker.getVisible()&&s.contains(k[r].marker.getPosition())){t.push(k[r])
}}return e(t)
}function e(s){var r=d.getCenter();
for(var t=0;
t<s.length;
t++){s[t].distanceToCenter=google.maps.geometry.spherical.computeDistanceBetween(s[t].marker.getPosition(),r)
}return s.sort(function(v,u){return v.distanceToCenter-u.distanceToCenter
})
}function m(s){somospnt.util.ui.places.unselect();
h.close();
i.setVisible(false);
if(s.geometry.viewport){d.fitBounds(s.geometry.viewport)
}else{d.setCenter(s.geometry.location);
d.setZoom(17)
}i.setPosition(s.geometry.location);
i.setVisible(true);
var r="";
if(s.address_components){r=[(s.address_components[0]&&s.address_components[0].short_name||""),(s.address_components[1]&&s.address_components[1].short_name||""),(s.address_components[2]&&s.address_components[2].short_name||"")].join(" ")
}h.setContent('<div class="myplaces-info"><strong>'+s.name+"</strong><br>"+r);
h.open(d,i)
}function l(v){j(k,true);
if(v.length){var s=[];
for(var u=0;
u<k.length;
u++){for(var t=0;
t<k[u].place.tags.length;
t++){if($.inArray(k[u].place.tags[t],v)!==-1){s.push(k[u]);
break
}}}var r=[];
for(var u=0;
u<k.length;
u++){if($.inArray(k[u],s)===-1){r.push(k[u])
}}j(r,false)
}b()
}function j(r,t){for(var s=0;
s<r.length;
s++){r[s].marker.setVisible(t)
}}function p(r){h.setContent(f.render(r.place));
h.open(d,r.marker);
r.infowindow=h
}function o(){var s=19;
var r=e(k)[0];
console.log(r);
while(!d.getBounds().contains(new google.maps.LatLng(r.place.lat,r.place.lng))){s=s-1;
d.setZoom(s)
}}return{init:q,focusOnGooglePlace:m,filterLocations:l,highlightPlace:p,zoomOutUntilMarkerAppear:o}
})();
somospnt.util.ui.places=(function(){var d,h;
var k="myplaces-place-";
var a,f;
function j(l,o,m,n){d=$.templates(l);
h=$.templates(o);
a=m;
f=n;
$(".myplaces-placesList ul").on("click","li",b);
$(".myplaces-infoContenido").append('<p class="myplaces-info-results">To see the details of a place please click on it.</p>')
}function b(){var m=$(this);
var l=m.data("location");
$(".myplaces-selectedPlaces").removeClass("myplaces-selectedPlaces");
m.addClass("myplaces-selectedPlaces");
$(".myplaces-infoContenido").empty().append(h.render(l.place));
somospnt.util.ui.map.highlightPlace(l);
f()
}function g(l){var n=$(".myplaces-selectedPlaces").data("location");
var o=$(".myplaces-placesList ul");
o.empty();
if(l.length){for(var m=0;
m<l.length;
m++){o.append($(d.render(l[m].place)).addClass(k+l[m].place.id).data("location",l[m]))
}}else{o.append('<p class="myplaces-info-results">No results to display in this area</p>')
}if(n){$("."+k+n.place.id).addClass("myplaces-selectedPlaces")
}a()
}function i(l){$("."+k+l).click()
}function e(){return $(".myplaces-selectedPlaces").data("location")
}function c(){$(".myplaces-selectedPlaces").removeClass("myplaces-selectedPlaces");
$(".myplaces-infoContenido").empty().append('<p class="myplaces-info-results">To see the details of a place please click on it.</p>')
}return{init:j,showLocations:g,selecteById:i,getSelectedLocation:e,unselect:c}
})();
somospnt.util.ui.searcher=(function(){var c,b,e;
function f(i,g){var h={componentRestrictions:{country:i}};
e=new google.maps.Geocoder(h);
b=$("."+g);
c=new google.maps.places.Autocomplete(b.get(0),h);
$(".myplaces-buscar").on("click",d);
b.on("keypress",function(k){var j=k.charCode?k.charCode:k.keyCode;
if(j===13){d()
}});
google.maps.event.addListener(c,"place_changed",a)
}function a(){b.removeClass("notfound");
var g=c.getPlace();
if(!g.geometry){b.addClass("notfound");
return
}somospnt.util.ui.map.focusOnGooglePlace(g);
somospnt.util.ui.map.zoomOutUntilMarkerAppear()
}function d(){var g=b.val();
e.geocode({address:g},function(i,h){if(h===google.maps.GeocoderStatus.OK){i[0].name=i[0].name||g;
somospnt.util.ui.map.focusOnGooglePlace(i[0]);
somospnt.util.ui.map.zoomOutUntilMarkerAppear()
}})
}return{init:f}
})();
(function(a){a.fn.extend({myplaces:function(c){var d=a(this);
var b={places:[],country:"AR",placeListTemplate:"<li>Enter a template for the list of places</li>",placeInfoTemplate:"<div>Enter a template for the information of the place</div>",popUpMapTemplate:"<div>Enter a template for the pop-up of the place</div>",mapOptions:{zoom:12,center:new google.maps.LatLng(-34.6090944,-58.389152),mapTypeId:google.maps.MapTypeId.ROADMAP},onLoad:function(){},onPlacesChange:function(){},onClickPlace:function(){},searcherInputClass:"myplaces-searcher-input",filterMainContainerClass:"myplaces-categories",filterContainerClass:"myplaces-filter-container",filterClass:"myplaces-check",filterActiveClass:"myplaces-check-on",filterTemplateId:"filtersTemplate",mainTemplateId:"mainTemplate"};
a.extend(b,c);
d.append("<div class='myplaces-spinner'></div>");
d.append(a.templates("#"+b.mainTemplateId).render());
somospnt.util.ui.places.init(b.placeListTemplate,b.placeInfoTemplate,b.onPlacesChange,b.onClickPlace);
somospnt.util.ui.map.init(b.places,b.popUpMapTemplate,b.mapOptions);
somospnt.util.ui.searcher.init(b.country,b.searcherInputClass);
somospnt.util.ui.filter.init(b);
b.onLoad();
d.find(".myplaces-spinner").remove()
}})
}(jQuery));
